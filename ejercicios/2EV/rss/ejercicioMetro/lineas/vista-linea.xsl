<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link 
                    rel='stylesheet' 
                    type='text/css' 
                    media='screen' 
                    href='css/main.css'/>

                <script>
                    function cargaEstacion(name){
                        var templateEstacion = document.querySelector("#estacion");
                        var cloneEstacion = document.importNode(templateEstacion.content, true);
                        cloneEstacion.querySelector("#nombre-estacion").textContent=name;
                        document.querySelector("#estaciones_linea").appendChild(cloneEstacion);
                    }

                </script>  
            </head>
            <body>

                <template id="estacion">
                    <div>
                        <h2 id="nombre-estacion"></h2>
                    </div>
                </template>

                <xsl:element name="h1">
                    <xsl:attribute name="style">
                        background:<xsl:value-of select="linea/color"/>
                    </xsl:attribute>
                    <xsl:value-of select="linea/@name"/>
                </xsl:element>

                <div id="estaciones_linea">
                    <ul>
                        <xsl:for-each select="linea/estaciones/estacion">

                            <script>
                                cargaEstacion("<xsl:value-of select="./name"/>")
                            </script>
                            <!-- <li>
                                <xsl:value-of select="./name"/>
                            </li> -->
                        </xsl:for-each>
                    </ul>
                </div>



                <!-- <h1 style="background: ">
                    <xsl:value-of select="linea/@name"/>
                </h1> -->

                

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>